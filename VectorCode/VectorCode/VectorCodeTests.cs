﻿using NUnit.Framework;
using System;

namespace VectorCode
{
    [TestFixture]
    public class VectorCodeTests
    {
        [Test]
        public void CreatingExceptions()
        {
            var vectorCode = new VectorCode(new int[]{0, 1, 2, 3, 4});
            Assert.Throws<IndexOutOfRangeException>( () => vectorCode.Insert(5, 5));  
            Assert.Throws<IndexOutOfRangeException>( () => vectorCode.Insert(-1, -1));  
            Assert.Throws<Exception>( () => vectorCode.ScalarProduct(new VectorCode(new int[]{})));  
            Assert.Throws<Exception>( () => vectorCode.Sum(new VectorCode(new int[]{}))); 
        }

        [Test]
        public void DecodeEmptyVector()
        {
            CheckVector(new VectorCode(new int[] {}).Decode(), new int[]{});
        }
        
        [Test]
        public void DecodeNotZeroVector()
        {
            CheckVector(new VectorCode(new int[] {0, 1, 2, 3}).Decode(), new int[]{0, 1, 2, 3});
            CheckVector(new VectorCode(new int[] {0, 0, 0, 0, 0, 0, 0, 0}).Decode(), new int[]{0, 0, 0, 0, 0, 0, 0, 0});
            CheckVector(new VectorCode(new int[] {1, 2, 3, 4, 5, 6, 7, 8}).Decode(), new int[]{1, 2, 3, 4, 5, 6, 7, 8});
        }

        [Test]
        public void InsertInZeroVector()
        {
            var vectorCode = new VectorCode(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
            vectorCode.Insert(1, 1);
            vectorCode.Insert(9, 9);
            vectorCode.Insert(5, 5);
            CheckVector(vectorCode.Decode(), new int[]{0, 1, 0, 0, 0, 5, 0, 0, 0, 9});
        }
        
        [Test]
        public void InsertInNotZeroVector()
        {
            var vectorCode = new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            vectorCode.Insert(9, 0);
            vectorCode.Insert(8, 1);
            vectorCode.Insert(7, 2);
            vectorCode.Insert(6, 3);
            vectorCode.Insert(5, 4);
            vectorCode.Insert(0, 5);
            vectorCode.Insert(4, 6);
            vectorCode.Insert(1, 9);
            vectorCode.Insert(2, 8);
            vectorCode.Insert(3, 7);
            CheckVector(vectorCode.Decode(), new int[]{9, 8, 7, 6, 5, 0, 4, 3, 2, 1});
        }

        [Test]
        public void DeleteFromZeroVector()
        {
            var vectorCode = new VectorCode(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
            vectorCode.Delete(0);
            vectorCode.Delete(3);
            vectorCode.Delete(4);
            vectorCode.Delete(9);
            vectorCode.Delete(8);
            CheckVector(vectorCode.Decode(), new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
        }
        
        [Test]
        public void DeleteFromNotZeroVector()
        {
            var vectorCode = new VectorCode(new int[]{9, 8, 7, 6, 5, 0, 4, 3, 2, 1});
            vectorCode.Delete(0);
            vectorCode.Delete(3);
            vectorCode.Delete(4);
            vectorCode.Delete(9);
            vectorCode.Delete(8);
            CheckVector(vectorCode.Decode(), new int[]{0, 8, 7, 0, 0, 0, 4, 3, 0, 0});
        }

        [Test]
        public void SumEmptyVectors()
        {
            var vectorCode1 = new VectorCode(new int[]{});
            var vectorCode2 = new VectorCode(new int[]{});
            CheckVector(vectorCode1.Sum(vectorCode2).Decode(), new int[]{});
        }

        [Test]
        public void SumVectors()
        {
            var vectorCode1 = new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            var vectorCode2 = new VectorCode(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0});
            CheckVector(vectorCode1.Sum(vectorCode2).Decode(), new int[]{9, 9, 9, 9, 9, 9, 9, 9, 9, 9});
        }

        [Test]
        public void ScalarProductZeroVector()
        {   
            var vectorCode1 = new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            var vectorCode2 = new VectorCode(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
            Assert.AreEqual(vectorCode1.ScalarProduct(vectorCode2), 0);
        }
        
        [Test]
        public void ScalarProductEmptyVectors()
        {
            var vectorCode1 = new VectorCode(new int[]{});
            var vectorCode2 = new VectorCode(new int[]{});
            Assert.AreEqual(vectorCode1.ScalarProduct(vectorCode2), 0);
        }

        [Test]
        public void ScalarProduct()
        {
            Assert.AreEqual(new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}).ScalarProduct(new VectorCode(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})), 120);
            Assert.AreEqual(new VectorCode(new int[]{0, 1, 2, 3, 4}).ScalarProduct(new VectorCode(new int[]{9, 0, 0, 6, 5})), 38);
        } 
        
        [Test]
        public void EmptyVectorSum()
        {
            CheckVector(new VectorCode(new int[]{}).VectorSum().Decode(), new int[]{});
        }
        
        [Test]
        public void VectorSum()
        {
            CheckVector(new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}).VectorSum().Decode(), new int[]{9, 17, 24, 30, 35, 39, 42, 44, 45, 45});
            CheckVector(new VectorCode(new int[]{1, 5, 0, 3, 2, 0, 66, 0, 0, 0}).VectorSum().Decode(), new int[]{0, 0, 0, 66, 66, 68, 71, 71, 76, 77});
            CheckVector(new VectorCode(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}).VectorSum().Decode(), new int[]{0, 1, 3, 6, 10, 15, 21, 28, 36, 45});
            CheckVector(new VectorCode(new int[]{10, 5, 0, 0, 2, 0, 0, 0, 30, 0}).VectorSum().Decode(), new int[]{0, 30, 30, 30, 30, 32, 32, 32, 37, 47});
            CheckVector(new VectorCode(new int[]{0, 0, 0, 0, 0, 0, 1}).VectorSum().Decode(), new int[]{1, 1, 1, 1, 1, 1, 1});
            CheckVector(new VectorCode(new int[]{1, 1, 1, 0, 0, 0, 0}).VectorSum().Decode(), new int[]{0, 0, 0, 0, 1, 2, 3});
            CheckVector(new VectorCode(new int[]{1, 0, 0, 0, 0, 0, 0}).VectorSum().Decode(), new int[]{0, 0, 0, 0, 0, 0, 1});
            CheckVector(new VectorCode(new int[]{1}).VectorSum().Decode(), new int[]{1});
            CheckVector(new VectorCode(new int[]{1, 2}).VectorSum().Decode(), new int[]{2, 3});
        }

        [Test]
        public void MultOnZero()
        {
            var vectorCode1 = new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            vectorCode1.Mult(9, 0);
            CheckVector(vectorCode1.Decode(), new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 0});
            vectorCode1.Mult(9, 10);
            vectorCode1.Mult(0, 11);
            CheckVector(vectorCode1.Decode(), new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 0});
        }
        
        [Test]
        public void MultNotExistingValue()
        {
            var vectorCode1 = new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            vectorCode1.Mult(10, 10);
            CheckVector(vectorCode1.Decode(), new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        }
        
        [Test]
        public void MultOnOne()
        {
            var vectorCode1 = new VectorCode(new int[]{9, 1, 2, 3, 4, 9, 6, 7, 8, 9});
            vectorCode1.Mult(9, 1);
            CheckVector(vectorCode1.Decode(), new int[]{9, 1, 2, 3, 4, 9, 6, 7, 8, 9});
        }
        
        [Test]
        public void Mult()
        {
            var vectorCode1 = new VectorCode(new int[]{9, 1, 2, 3, 4, 9, 6, 7, 8, 9});
            vectorCode1.Mult(9, 9);
            CheckVector(vectorCode1.Decode(), new int[]{81, 1, 2, 3, 4, 81, 6, 7, 8, 81});
            vectorCode1.Mult(2, 20);
            CheckVector(vectorCode1.Decode(), new int[]{81, 1, 40, 3, 4, 81, 6, 7, 8, 81});
        }

        public void CheckVector(int[] vector, int[] result)
        {
            Assert.AreEqual(vector.Length, result.Length);
            for (int i = 0; i < vector.Length; i++)
                Assert.AreEqual(vector[i], result[i]);
        }
    }
}
