﻿using System;

namespace VectorCode
{
    public class MainClass
    {
        public static void Main(string[] args)
        { 
            VectorCode vector = new VectorCode(new int[] {0, 1, 2, 3, 0, 4, 0, 4, 5, 0, 6, 6});

            var decodedVector = vector.Decode();
            Console.WriteLine($"Vector: {ArrayToString(decodedVector)}");
            
            vector.Insert(6, 9);
            Console.WriteLine($"Insert result: {ArrayToString(vector.Decode())}");
            
            vector.Delete(8);
            Console.WriteLine($"Deletion result: {ArrayToString(vector.Decode())}");
            
            var vectorLength = vector.Length.ToString();
            Console.WriteLine($"Length: {vectorLength}");
            
            vector.Mult(4, 5);
            Console.WriteLine($"Mult result: {ArrayToString(vector.Decode())}");

            Console.WriteLine($"CURRENT: {ArrayToString(vector.Decode())}");
            
            var scalar = vector.ScalarProduct(new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
            Console.WriteLine($"Scalar product: {scalar}");
            
            var newVector = vector.VectorSum();
            Console.WriteLine($"VectorSum result: {ArrayToString(newVector.Decode())}");
            
            newVector = vector.Sum(new VectorCode(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
            Console.WriteLine($"Sum result: {ArrayToString(newVector.Decode())}");
        }

        public static string ArrayToString(int[] array) => String.Join(" ", array);
    }
}
