﻿using System;
using System.Collections.Generic;
namespace VectorCode.Properties
{
    public class VectorCode
    {
        List<Tuple<int, int>> Components;
        public int Lenght { get; }

        public VectorCode(int[] vector)
        {
            Components = new List<Tuple<int, int>>();
            Lenght = vector.Length;
            for (int i = 0; i < vector.Length; i++)
                if (vector[i] != 0)
                    Components.Add(new Tuple<int, int>(i, vector[i]));
        }

        public int[] Decode()
        {
            int[] result = new int[Lenght];
            foreach (var component in Components)
                result[component.Item1] = component.Item2;
            return result;
        }

        public void Insert(int k, int pos)
        {
            if (pos >= Lenght)
                throw new IndexOutOfRangeException();
            for (int i = 0; i < Components.Count; i++)
                if (Components[i].Item1 == pos)
                    Components[i] = new Tuple<int, int>(pos, k);
        }

        public void Delete(int pos)
        {
            throw new NotImplementedException();   
        }

        public int ScalarProduct(VectorCode v)
        {
            throw new NotImplementedException();   
        }

        public VectorCode Sum(VectorCode v)
        {
            throw new NotImplementedException();
        }

        public VectorCode VectorSum()
        {
            throw new NotImplementedException();
        }

        public void Mult(int a, int c)
        {
            throw new NotImplementedException();
        }
    }
}
