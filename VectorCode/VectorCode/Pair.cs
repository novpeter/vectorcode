﻿// Pair.cs
using System;
namespace VectorCode
{
    public class Pair
    {
        public int Index { get; }
        public int Value { get; }
        
        public Pair(int index, int value)
        {
            Index = index;
            Value = value;
        }
    }
}
