﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

namespace VectorCode
{
    public class Node<T>
    {
        public T Value { get; }

        public Node (T value) => Value = value;

        public Node<T> Next { get; set; }
    }
    
    public class LinkedList<T> : IEnumerable<T>
    {
        public Node<T> Head { get; private set; }
        public Node<T> Tail { get; private set; }
        public bool IsEmpty { get { return Head == null; } }

        public LinkedList() => Head = Tail = null;

        public Node<T> AddFirst(T value)
        {
            var newNode = new Node<T>(value);
            if (Head == null) 
                Head = Tail = newNode;
            else
            {
                newNode.Next = Head;
                Head = newNode;
            }
            return newNode;
        }
        
        public Node<T> Insert(T value)
        {
            var newNode = new Node<T>(value);
            if (Head == null)  
                Head = Tail = newNode;
            else
            {
                Tail.Next = newNode;
                Tail = newNode;
            }
            return newNode;
        }

        public Node<T> AddAfter(Node<T> prevNode, T value)
        {
            if (prevNode == null)
                return null;
            if (prevNode == Tail)
                return Insert(value);
            var newNode = new Node<T>(value);
            newNode.Next = prevNode.Next;
            prevNode.Next = newNode;
            return newNode;
        }
        
        public void Remove(Node<T> prevNode, Node<T> node)
        {
            if (node == null) 
                return;
            if (node == Head)
                RemoveFirst();
            if (prevNode != null)
                prevNode.Next = node.Next;
            node.Next = null;
        }

        public void RemoveFirst()
        {
            if (Head != null)
            {
                var newHead = Head.Next;
                Head.Next = null;
                Head = newHead;
            }
        }
        
        public override string ToString()
        {
            var result = new StringBuilder();
            var current = Head;
            while (current != null)
            {
                result.Append(current.Value.ToString() + " ");
                current = current.Next;
            }
            return result.ToString();
        }

        public IEnumerator<T> GetEnumerator() 
        {
            var current = Head;
            while(current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
