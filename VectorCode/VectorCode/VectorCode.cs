﻿using System;

namespace VectorCode
{
    public class VectorCode
    {
        LinkedList<Pair> Components;
        public int Length { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:VectorCode.VectorCode"/> class.
        /// </summary>
        /// <param name="length">Length on Vector.</param>
        public VectorCode(int length)
        {
            Components = new LinkedList<Pair>();
            Length = length;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:VectorCode.VectorCode"/> class.
        /// </summary>
        /// <param name="vector">Vector.</param>
        public VectorCode(int[] vector)
        {
            Components = new LinkedList<Pair>();
            Length = vector.Length;
            for (int i = 0; i < vector.Length; i++)
                if (vector[i] != 0)
                    Components.Insert(new Pair(i, vector[i]));
        }

        public int[] Decode()
        {
            int[] result = new int[Length];
            foreach (var component in Components)
                result[component.Index] = component.Value;
            return result;
        }

        /// <summary>
        /// Inserts the specified k on given position.
        /// </summary>
        /// <param name="k">Value.</param>
        /// <param name="pos">Position.</param>
        public void Insert(int k, int pos)
        {
            if (pos >= Length || pos < 0)
                throw new IndexOutOfRangeException("Wrong position");
            Node<Pair> current = Components.Head;
            Node<Pair> previous = null;
            while (current != null)
            {
                if (pos < current.Value.Index)
                {
                    if (previous == null) Components.AddFirst(new Pair(pos, k));
                    else Components.AddAfter(previous, new Pair(pos, k));
                    return;
                }
                if (pos == current.Value.Index)
                {
                    if (k != 0)
                        Components.AddAfter(current, new Pair(pos, k));
                    Components.Remove(previous, current);
                    return;
                }
                previous = current;
                current = current.Next;
            }
            if (k != 0)    
                Components.Insert(new Pair(pos, k));
        }

        /// <summary>
        /// Deletes the specified pos.
        /// </summary>
        /// <param name="pos">Position.</param>
        public void Delete(int pos)
        {
            if (pos >= Length || pos < 0)
                throw new IndexOutOfRangeException("Wrong position");
            Node<Pair> current = Components.Head;
            Node<Pair> previous = null;
            while (current != null)
            {
                if (pos == current.Value.Index)
                {
                    Components.Remove(previous, current);
                    return;
                }  
                if (pos < current.Value.Index) return;
                previous = current;
                current = current.Next; 
            }
        }

        /// <summary>
        /// Scalars the product.
        /// </summary>
        /// <returns>The product.</returns>
        /// <param name="v">VectorCode</param>
        public int ScalarProduct(VectorCode v)
        {
            if (v.Length != Length) throw new Exception("Different length");
            int result = 0;
            var current1 = Components.Head;
            var current2 = v.Components.Head;
            while (current1 != null && current2 != null)
            {
                if (current1.Value.Index == current2.Value.Index)
                {
                    result += current1.Value.Value * current2.Value.Value; 
                    current1 = current1.Next;
                    current2 = current2.Next;
                }
                else if (current1.Value.Index < current2.Value.Index)
                    current1 = current1.Next;
                else 
                    current2 = current2.Next;
            }
            return result;
        }

        /// <summary>
        /// Sum the specified v.
        /// </summary>
        /// <returns>The sum.</returns>
        /// <param name="v">Vector</param>
        public VectorCode Sum(VectorCode v)
        {
            if (v.Length != Length) throw new Exception("Different length");
            var resultVector = new VectorCode(Length);
            var cur1 = Components.Head;
            var cur2 = v.Components.Head;
            while (cur1 != null && cur2 != null)
            {
                var index1 = cur1.Value.Index;
                var index2 = cur2.Value.Index;
                if (index1 == index2)
                { 
                    resultVector.Insert(cur1.Value.Value + cur2.Value.Value, index1);
                    cur1 = cur1.Next;
                    cur2 = cur2.Next;
                }
                else if (index1 < index2)
                {
                    resultVector.Insert(cur1.Value.Value, index1);
                    cur1 = cur1.Next;
                }
                else
                {
                    resultVector.Insert(cur2.Value.Value, index2);
                    cur2 = cur2.Next;
                }
            }
            while(cur1 != null)
            {
                resultVector.Insert(cur1.Value.Value, cur1.Value.Index);
                cur1 = cur1.Next;
            }
            while(cur2 != null)
            {
                resultVector.Insert(cur2.Value.Value, cur2.Value.Index);
                cur2 = cur2.Next;
            }
            return resultVector;
        }
        
        /// <summary>
        /// Constructs a new vector whose I-th component is the sum of the last I components of the original vector.
        /// </summary>
        /// <returns>The new Vector</returns>
        public VectorCode VectorSum()
        {
            var resultVector = new VectorCode(Length);
            var reversedVectorComponents = ReverseVectorComponents();
            var current = reversedVectorComponents.Head;
            if (current == null) return resultVector;
            var sum = 0;
            var lastIndex = Length - 1;
            while (current != null)
            {   
                var index = Length - current.Value.Index - 1;
                if (index == Length) break ;
                if (index - lastIndex != 1)
                    for (int i = lastIndex + 1; i < index; i++)
                        resultVector.Insert(sum, i);
                sum += current.Value.Value;
                resultVector.Insert(sum, index);
                lastIndex = index;
                current = current.Next ;
            }
            if (lastIndex != Length - 1)
                for (int i = lastIndex + 1; i < Length; i++)
                    resultVector.Insert(sum, i);
            return resultVector;
        }

        /// <summary>
        /// Reverses the vector components.
        /// </summary>
        /// <returns>The vector components.</returns>
        private LinkedList<Pair> ReverseVectorComponents()
        {
            var result = new LinkedList<Pair>();
            var current = Components.Head;
            while (current != null)
            {
                result.AddFirst(current.Value);
                current = current.Next;
            }
            return result;
        }

        /// <summary>
        /// Mult the specified a on c.
        /// </summary>
        /// <param name="a">The alpha component.</param>
        /// <param name="c">Multiplier.</param>
        public void Mult(int a, int c)
        {
            if (a == 0 || c == 1 || Length == 0) return;
            Node<Pair> current = Components.Head;
            Node<Pair> previous = null;
            while (current != null)
            {
                if (current.Value.Value == a)
                {
                    var newComponent = Components.AddAfter(current, new Pair(current.Value.Index, a * c));
                    Components.Remove(previous, current);
                    current = newComponent;
                }
                previous = current;
                current = current.Next;
            }
        }
        
        public override string ToString() => Components.ToString();
    }
}
